import React, { Component } from 'react';
import './login.styles.css';
import {Link} from 'react-router-dom';
import api from  '../../api';
import Alert from '../../common/alert.component';

class Login extends Component {
  
    state = {
        error:{
         email:"0",
         password:"0"
         },
         form_alert:null
    }

  
    //handle form submit
    handleSubmit =(e)=>{
    e.preventDefault();
   
    if(!this.validate_form(this.state.error)){
        this.setState({
            form_alert:"Please fill form first"
        })
        
     }else{
    let fornmData = {
    "email":e.target.elements.email.value,
    "password":e.target.elements.password.value
    }
    let url = api.url+api.endpoint.signin;
    fetch(url,{
        method:'POST',
        headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(fornmData)
    })
    .then((res)=>res.json())
    .then((result)=>{
        if(result.status===200){
           console.log(result);
        }else{
            this.setState({
                form_alert:result.message
            })
            
        }
    })
}
    }
        //to close alert 
    handleClose = ()=> {
        this.setState({ form_alert: null });  
      }
   
//to check form validation
    handleChange = (e)=>{
        e.preventDefault();
        const email_regex = new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g);
        const {name,value} = e.target;
        let errors = this.state.error;
        switch(name){
        case 'email':
            errors.email = !email_regex.test(value)?"Enter a valid Email Address":"";
            break;
            case 'password':
                errors.password = value.length<8?"Password must have 8 character long":"";
            break;
            default:
            break;
        }
        this.setState({errors,[name]:value})
    }
//to vaidate form
    validate_form = (errors)=>{
        let valid = true;
        Object.values(errors).forEach(
            (val)=>val.length>0 && (valid=false)
        );
        return valid;
            }  
    render(){
        return (
            <div className="container-fluid BackGround">
                <div className="row   formRow">
                <div className="col-md-6"></div>
                <div className="col-md-6 mx-auto justify-content-center  part-2">
                    <div className="card Login-card">
                    <Alert handleclose = {this.handleClose.bind(this)} c_name={this.state.form_alert===null?"alert alert-danger alert-dismissible fade hide":"alert alert-danger alert-dismissible fade show"} >{this.state.form_alert}</Alert>
                        <h3 className=" text-blue font-20">Sign in</h3>
                    <form method="post" className="registerForm"  onSubmit={this.handleSubmit.bind(this)}>
                     <div className="form-row">
                            <div className="form-group col-md-12">
                            <label>E-mail Address <span>*</span></label>
                                <input autoComplete="off" onChange={this.handleChange.bind(this)} className="form-control"   id="email" name="email"/>
                                <span className={this.state.error.email==='0'?'validate':"no-validate"}>{this.state.error.email}</span>
                                </div>
                            </div>
                        <div className="form-row">
                            <div className="form-group col-md-12">
                            <label>Password <span>*</span></label>
                                <input autoComplete="off" onChange={this.handleChange.bind(this)} type="password" className="form-control"  id="password" name="password"/>
                                <span className={this.state.error.password==='0'?'validate':"no-validate"}>{this.state.error.password}</span>
                                </div>
                            </div>
                        <div className="form-row">
                            <div className="form-group col-md-12 text-center">
                                <button className="BTn-Blue" type="submit">Sign in</button>
                                
                            </div>
     </div>
                    </form>
                    <div className="row">
                    <div className="col-md-12 text-center">
                        <Link to="#" className="ForgotPAssword">Forgot Password?</Link>
                            
                            </div>
                    </div>
                    <div className="row registerRow">
                    
                            <div className="col-md-12 d-flex  align-items-center justify-content-center">
                            <span className="mr-3 ">New Vendors?</span>
                                <Link  to="/vendor/signup" className="BTn-Blue BTn-Blue-register">SIGN UP</Link>
                            </div>
                        </div>
                    </div>    
                </div>
                </div>
            </div>
            
        )
    }
}



export default Login;