import React, { Component } from 'react';
import './login.styles.css';
import {Link} from 'react-router-dom';
import api from  '../../api';
import Alert from '../../common/alert.component';

class signup extends Component {

    state = {
        error:{
         fullName:"0",
         email:"0",
         mobileNo:"0",
         password:"0"
         },
         form_alert:null
    }

    //handle form submit
    handleSubmit =(e)=>{
    e.preventDefault();
    if(!this.validate_form(this.state.error)){
       this.setState({
           form_alert:"Please fill form first"
       })
     
    }else{
        let fornmData = {
            "fullName":e.target.elements.fullName.value,
        "email":e.target.elements.email.value,
        "password":e.target.elements.password.value,
        "mobileNo":e.target.elements.mobileNo.value
        }
        let url = api.url+api.endpoint.signup;
        fetch(url,{
            method:'POST',
            headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify(fornmData)
        })
        .then((res)=>res.json())
        .then((result)=>{
            if(result.status===200){
                window.location ='/vendor/signin';
            }else{
                this.setState({
                    form_alert:result.message
                })
               
            }
        })
    }
    
    }

    handleClose = ()=> {
    this.setState({ form_alert: null });  
  }

    //to check form validation
    handleChange = (e)=>{
        e.preventDefault();
        const email_regex = new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g);
        const {name,value} = e.target;
        let errors = this.state.error;
        switch(name){
        case 'fullName':
            errors.fullName = value.length===0?"Please fill your Full Name":"";
            break;
        case 'email':
            errors.email = !email_regex.test(value)?"Enter a valid Email Address":"";
            break;
            case 'mobileNo':
                errors.mobileNo = isNaN(value)?"Mobile Number contains numeric values":value.length<10?"Mobile Number must have 10 character long":"";
            break;
            case 'password':
                errors.password = value.length<8?"Password must have 8 character long":"";
            break;
            default:
            break;
        }
        this.setState({errors,[name]:value})
        }

    validate_form = (errors)=>{
    let valid = true;
    Object.values(errors).forEach(
    (val)=>val.length>0 && (valid=false)
    );
    return valid;
    }    

    render(){
        return (
            <div className="container-fluid BackGround">
                <div className="row   formRow">
                <div className="col-md-6"></div>
                <div className="col-md-6 mx-auto justify-content-center  part-2 ">
                    <div className="card Login-card">
        <Alert handleclose = {this.handleClose.bind(this)} c_name={this.state.form_alert===null?"alert alert-danger alert-dismissible fade hide":"alert alert-danger alert-dismissible fade show"} >{this.state.form_alert}</Alert>
                        <h3 className=" text-blue font-20">Sign up</h3>
                    <form method="post" autoComplete="off" className="registerForm"  onSubmit={this.handleSubmit.bind(this)}>
                        <div className="form-row">
                            <div className="form-group col-md-12">
                                <label>Full Name <span>*</span></label>
                                <input autoComplete="off" onChange={this.handleChange.bind(this)} className="form-control"   id="fullName" name="fullName"/>
                                <span className={this.state.error.fullName==='0'?'validate':"no-validate"}>{this.state.error.fullName}</span>
                                </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-12">
                            <label>E-mail Address <span>*</span></label>
                                <input autoComplete="off" onChange={this.handleChange.bind(this)} className="form-control"   id="email" name="email"/>
                                <span className={this.state.error.email==='0'?'validate':"no-validate"}>{this.state.error.email}</span>
                                </div>
                            </div>
                        <div className="form-row">
                            <div className="form-group col-md-12">
                            <label>Mobile Number <span>*</span></label>
                                <input autoComplete="off" onChange={this.handleChange.bind(this)} maxLength="10" className="form-control"   id="mobileNo" name="mobileNo"/>
                                <span className={this.state.error.mobileNo==='0'?'validate':"no-validate"}>{this.state.error.mobileNo}</span>
                                 </div>
                            
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-12">
                            <label>Password <span>*</span></label>
                                <input autoComplete="off" onChange={this.handleChange.bind(this)} type="password" className="form-control"  id="password" name="password"/>
                                <span className={this.state.error.password==='0'?'validate':"no-validate"}>{this.state.error.password}</span>
                                </div>
                            </div>
                        <div className="form-row">
                            <div className="form-group col-md-12 text-center">
                                <button className="BTn-Blue" type="submit">Create Your Account</button>
                                
                            </div>
                           
                            
                        </div>
                    </form>
                    <div className="row registerRow">
                            <div className="col-md-12 text-center">
                                <Link  to="/vendor/signin" className="BTn-Blue BTn-Blue-register" >SIGN IN</Link>
                            </div>
                        </div>
                    </div>    
                </div>
                </div>
            </div>
            
        )
    }
}



export default signup;