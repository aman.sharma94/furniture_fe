import React from 'react';
import signUp from './Vendor/Login/signup.component';
import signin from './Vendor/Login/signin.component';
import {BrowserRouter,Route} from 'react-router-dom';

function App() {
  return (
<BrowserRouter>
<Route path="/vendor/signup" component={signUp}></Route>
<Route path="/vendor/signin" component={signin}></Route>
</BrowserRouter>
  );
}

export default App;
