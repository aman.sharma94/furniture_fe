import React, { Component } from 'react';


class Alert extends Component{

    render(){
        return(
            <div className={this.props.c_name} role="alert">{this.props.children}
  <button type="button" className="close"  onClick={this.props.handleclose} >
    <span aria-hidden="true">&times;</span>
  </button>
</div>
        )
    }
}

export default Alert